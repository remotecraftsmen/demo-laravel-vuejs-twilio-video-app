import Vue from 'vue';
import App from './components/App.vue';
import VueRouter from 'vue-router';
import {routes} from './routes';
import {store} from './store';
import Axios from 'axios';

Vue.use(VueRouter);
const router = new VueRouter({
    mode: 'history',
    routes
});

Vue.prototype.$http = Axios;

Vue.config.devtools = true;
Vue.config.performance = true;

const app = new Vue({
    el: '#app',
    store,
    router,
    render: h => h(App)
});
