import RoomList from './components/RoomList.vue';
import Home from './components/Home.vue';
import Room from './components/Room.vue';

export const routes = [
    {path: '/', component: Home},
    {path: '/room', component: RoomList},
    {path: '/room/:roomName', component: Room},
];
