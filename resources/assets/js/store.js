import Vue from 'vue';
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        activeRoom: null,
        nickName: ''
    },
    getters: {
        nickName: state => {
            return state.nickName;
        }
    },
    mutations: {
        setNickName: (state, payload) => {
            state.nickName = payload;
        }
    }
    ,
    actions: {
        setNickName({commit}, payload) {
            commit('setNickName', payload);
        }
    }
});
