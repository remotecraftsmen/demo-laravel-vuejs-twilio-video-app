# laravel-vue-twilio-video-demo

# Laravel and Vue - Twilio Video Chat
Basic todo app written in Slim

### Prerequisites
* Docker - v18.06.1

###Installation

##Clone the repository
`git clone git@bitbucket.org:remotecraftsmen/laravel-vue-twilio-video-demo.git`

##Twilio configuration
- Sign up on [Twilio](https://www.twilio.com/).
- The TWILIO_ACCOUNT_SID and TWILIO_ACCOUNT_TOKEN can be found in your [Project Dashboard](https://www.twilio.com/console)
- Create an API key in the [Programmable Video](https://www.twilio.com/console/video/runtime/api-keys) section.
- Put all the keys into .env file

##Docker Setup
    ./exec.sh configure
    
##Start docker containers
    ./exec.sh up

##Stop docker containers 
    ./exec.sh down
   
##Using composer with docker
    ./exec.sh composer *
For example : 
    ./exec.sh composer update
    

### DEMO
Live demo available at https://laravel-vue-twilio-video-demo.rmtcfm.com
