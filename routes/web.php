<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');

Route::prefix('api')->group(function() {
    Route::prefix('room')->group(function() {
        Route::get('/', "VideoRoomsController@index");
        Route::get('/{roomName}', "VideoRoomsController@getSingleRoom");
        Route::post('/join', 'VideoRoomsController@joinRoom');
        Route::get('/participants/{roomName}', "VideoRoomsController@getRoomParticipants");

    });
});

Route::get('/{vue?}', function () {
    return view('home');
})->where('vue', '[\/\w\.-]*')->name('try');
