<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;
use \Auth;


class VideoRoomsController extends Controller
{
    protected $sid;
    protected $token;
    protected $key;
    protected $secret;

    public function __construct()
    {
        $this->sid = config('services.twilio.sid');
        $this->token = config('services.twilio.token');
        $this->key = config('services.twilio.key');
        $this->secret = config('services.twilio.secret');
    }

    public function index()
    {
        $rooms = [];
        try {
            $client = new Client($this->sid, $this->token);
            //$allRooms = $client->video->rooms->read([]);
            $rooms = $client->video->rooms
                ->read(["status" => "in-progress"]);

            $allRooms = array_map(function ($room) {
                return $room->uniqueName;
            }, $rooms);
            $allRooms = array_unique($allRooms);

        } catch (\Exception $e) {
            echo "Error: " . $e->getMessage();
        }

        return ['rooms' => $allRooms];
    }

    public function joinRoom(Request $request)
    {
        $roomName = $request->roomName;
        $identity = $request->userName;

        $token = new AccessToken($this->sid, $this->key, $this->secret, 3600, $identity);

        $videoGrant = new VideoGrant();
        $videoGrant->setRoom($roomName);

        $token->addGrant($videoGrant);

        return ['token' => $token->toJWT(), 'roomName' => $roomName, 'identity' => $identity];
    }

    public function getRoomParticipants($roomName)
    {
        $client = new Client($this->key, $this->secret);
        $participantsList = [];

        $participants = $client->video->rooms($roomName)->participants->read(array("status" => "connected"));

        foreach ($participants as $participant) {
            array_push($participantsList, $participant->identity);
        }

        return $participantsList;
    }

    public function getSingleRoom($roomName)
    {
        $client = new Client($this->sid, $this->token);

        $room = $client->video->v1->rooms($roomName)
            ->fetch();

        return $room;
    }

}
